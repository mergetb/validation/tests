module gitlab.com/mergetb/validation/tests

go 1.14

require (
	github.com/stretchr/testify v1.5.1
	gitlab.com/mergetb/engine v0.5.6
	gitlab.com/mergetb/xir v0.2.13
)
