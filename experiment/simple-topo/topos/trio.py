import mergexp as mx

net = mx.Topology('trio')
a = net.device('a')
b = net.device('b')
c = net.device('c')
net.connect([a, b, c])

mx.experiment(net)
