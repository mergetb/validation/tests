package main

import (
	"io/ioutil"
	"testing"

	"gitlab.com/mergetb/engine/pkg/model"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

func loadExperiment(t *testing.T, path string) *xir.Net {

	model.DumpScriptDir = "."

	src, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}

	topo, err := model.CompileToXir(string(src))
	if err != nil {
		t.Fatal(err)
	}

	return topo

}
