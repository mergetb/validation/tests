package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSingleNode(t *testing.T) {

	topo := loadExperiment(t, "topos/solo.py")

	assert.Equal(t, 1, len(topo.AllNodes()), "should be one node in topology")

}

func TestDuo(t *testing.T) {

	topo := loadExperiment(t, "topos/duo.py")

	assert.Equal(t, 2, len(topo.AllNodes()), "should be one node in topology")
	assert.Equal(t, 1, len(topo.AllLinks()), "should be one node in topology")

	assert.Equal(
		t,
		topo.AllNodes()[0].Neighbors()[0].Parent,
		topo.AllNodes()[1],
		"nodes should be connected",
	)

}

func TestTrio(t *testing.T) {

	topo := loadExperiment(t, "topos/trio.py")

	assert.Equal(t, 3, len(topo.AllNodes()), "should be one node in topology")
	assert.Equal(t, 1, len(topo.AllLinks()), "should be one node in topology")

	assert.Contains(
		t,
		topo.AllNodes()[0].Neighbors(),
		topo.AllNodes()[1].Endpoints[0],
		"a and b should be connected",
	)

	assert.Contains(
		t,
		topo.AllNodes()[0].Neighbors(),
		topo.AllNodes()[2].Endpoints[0],
		"a and c should be connected",
	)

	assert.Contains(
		t,
		topo.AllNodes()[1].Neighbors(),
		topo.AllNodes()[2].Endpoints[0],
		"b and c should be connected",
	)

}

func TestQuartet(t *testing.T) {

	topo := loadExperiment(t, "topos/quartet.py")

	assert.Equal(t, 4, len(topo.AllNodes()), "should be one node in topology")
	assert.Equal(t, 2, len(topo.AllLinks()), "should be one node in topology")

	assert.Contains(
		t,
		topo.AllNodes()[0].Neighbors(),
		topo.AllNodes()[1].Endpoints[0],
		"a and b should be connected",
	)

	assert.Contains(
		t,
		topo.AllNodes()[0].Neighbors(),
		topo.AllNodes()[2].Endpoints[0],
		"a and c should be connected",
	)

	assert.Contains(
		t,
		topo.AllNodes()[1].Neighbors(),
		topo.AllNodes()[2].Endpoints[0],
		"b and c should be connected",
	)

	assert.Equal(
		t,
		topo.AllNodes()[3].Neighbors()[0].Parent,
		topo.AllNodes()[2],
		"nodes d and c should be connected",
	)

}
