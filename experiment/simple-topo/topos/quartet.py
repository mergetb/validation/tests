import mergexp as mx

net = mx.Topology('trio')
a = net.device('a')
b = net.device('b')
c = net.device('c')
net.connect([a, b, c])

d = net.device('d')
net.connect([c, d])

mx.experiment(net)
