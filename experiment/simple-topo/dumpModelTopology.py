#!/usr/bin/env python3

from sys import stdin, stderr, exit
from json import dumps
import mergexp

# grab the well-known module-level global that has the topology.
from mergexp import __xp   

# execute the given python model script.
try:
    exec(stdin.read())
except Exception as e:
    print("Exception: {}".format(e), file=stderr)
    exit(10)

# make sure the __xp instance is OK.
if not isinstance(__xp, dict):
    print("bad mergexp library", file=stderr)
    exit(20)

# find the single Topology in __xp
topo = None
for v in __xp.values():
    if isinstance(v, mergexp.elements.Topology):
        topo = v
        break

if topo is None:
    print('no topology found', file=stderr)
    exit(30)

print(dumps(topo.xir(), indent=2))
exit(0)
